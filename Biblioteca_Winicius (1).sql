-- phpMyAdmin SQL Dump
-- version 5.3.0-dev
-- https://www.phpmyadmin.net/
--
-- Host: 192.168.30.23
-- Tempo de geração: 05/10/2022 às 15:39
-- Versão do servidor: 8.0.18
-- Versão do PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `Biblioteca Winicius`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `Autores`
--

CREATE TABLE `Autores` (
  `ID` int(11) NOT NULL,
  `Nome` varchar(50) NOT NULL,
  `Data de nascimento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Editoras`
--

CREATE TABLE `Editoras` (
  `ID` int(11) NOT NULL,
  `Nome` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Livros`
--

CREATE TABLE `Livros` (
  `ID` int(11) NOT NULL,
  `Título` varchar(50) NOT NULL,
  `Data` date NOT NULL,
  `ID Autor` int(11) NOT NULL,
  `ID Editora` int(11) NOT NULL,
  `ID Local` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Locais`
--

CREATE TABLE `Locais` (
  `ID` int(11) NOT NULL,
  `Estante` int(11) NOT NULL,
  `Plateleira` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
